//
//  NIXView.m
//  NIXNavigationController
//
//  Created by admin on 14.11.14.
//  Copyright (c) 2014 admin. All rights reserved.
//

#import "NIXView.h"

@implementation NIXView

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    if (![self isUserInteractionEnabled] || [self isHidden] || [self alpha] == 0)
    {
        return nil;
    }
    
    UIView *hitView = self;
    
    if (![self pointInside:point withEvent:event])
    {
        if (self.clipsToBounds)
        {
            return nil;
        }
        else
        {
            hitView = nil;
        }
    }
    
    for (UIView *subview in [[self subviews] reverseObjectEnumerator])
    {
        CGPoint insideSubviews = [self convertPoint:point toView:subview];
        UIView *sview = [subview hitTest:insideSubviews withEvent:event];
        
        if (sview)
        {
            return sview;
        }
    }
    
    return hitView;
}

@end
