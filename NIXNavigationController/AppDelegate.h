//
//  AppDelegate.h
//  NIXNavigationController
//
//  Created by admin on 14.11.14.
//  Copyright (c) 2014 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NIXViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NIXViewController *viewController;
@property (strong, nonatomic) UINavigationController *navController;

@end

