
//
//  NIXViewController.m
//  NIXNavigationController
//
//  Created by admin on 14.11.14.
//  Copyright (c) 2014 admin. All rights reserved.
//

#import "NIXViewController.h"
#import "NIXView.h"

@interface NIXViewController ()<NIXViewDelegate,UITextFieldDelegate>

@property (strong, nonatomic) UITextField *textField;

@end

@implementation NIXViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *createBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Create"
                                                                            style:UIBarButtonItemStylePlain
                                                                           target:self
                                                                           action:@selector(createAction)];
    
    UIBarButtonItem *cancelBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
                                                                            style:UIBarButtonItemStylePlain
                                                                           target:self
                                                                           action:@selector(cancelAction)];
    
    [[self navigationItem] setLeftBarButtonItem:cancelBarButtonItem];
    [[self navigationItem] setRightBarButtonItem:createBarButtonItem];
    
    _textField = [[UITextField alloc]initWithFrame:CGRectMake(100, 11, 170, 25)];
    [_textField setBackgroundColor:[UIColor lightGrayColor]];
    [_textField setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    [_textField setReturnKeyType:UIReturnKeyDone];
    [_textField setDelegate:self];
    [_textField setClearButtonMode:UITextFieldViewModeWhileEditing];
    [_textField setBorderStyle:UITextBorderStyleRoundedRect];
    
    [[[self navigationController] navigationBar] addSubview:_textField];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [[self view] addGestureRecognizer:tap];
}

#pragma mark - Action Button

- (void)createAction
{
    NSInteger theInteger = [_textField.text intValue];
    
    if ([_textField.text intValue] > 7)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Incorrect value!"
                                                        message:@"Number must be < 8!"
                                                       delegate:nil
                                              cancelButtonTitle:@"Try again"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        UIView *rootView = [self view];
        
        for (NSUInteger index = 0; index < theInteger; ++index)
        {
            NIXView *viewOnScreen = [[NIXView alloc] initWithFrame:CGRectMake(20, 20, 320 - 50 *index, 400 - 50 *index)];
            
            UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                         action:@selector(handleTap:)];
            
            [viewOnScreen setCenter:CGPointMake(rootView.frame.size.width / 2,
                                                rootView.frame.size.height / 2)];
            
            [viewOnScreen setBackgroundColor:[self randomColor]];
            [viewOnScreen setDelegate:self];
            [viewOnScreen setTag:index];
            [viewOnScreen setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
            [viewOnScreen addGestureRecognizer:tapGesture];
            [rootView addSubview:viewOnScreen];
            
            rootView = viewOnScreen;
        }
        
        [_textField resignFirstResponder];
        
        NSLog(@"create Action");
    }
}
- (void)cancelAction
{
    for (UIView *view in [[self view] subviews])
    {
        [view removeFromSuperview];
    }
    
    [_textField resignFirstResponder];
    
    NSLog(@"cancel Action");
}

#pragma mark - Private methods

- (void)handleTap:(UITapGestureRecognizer *)tapGesture
{
    [self animationMove:tapGesture.view];
}

- (CGFloat)randomFromZeroToOne
{
    return (float)(arc4random() % 256) / 255;
}

- (UIColor *)randomColor
{
    CGFloat r = [self randomFromZeroToOne];
    CGFloat g = [self randomFromZeroToOne];
    CGFloat b = [self randomFromZeroToOne];
    
    return [UIColor colorWithRed:r green:g blue:b alpha:1.f];
}

#pragma mark - UITextFieldDelegate methods

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self createAction];
    
    return YES;
}

- (BOOL)textField:(UITextField *)theTextField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (![string length] || [string intValue])
    {
        return YES;
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Incorrect value!"
                                                    message:@"You can type only number!"
                                                   delegate:nil
                                          cancelButtonTitle:@"Try again"
                                          otherButtonTitles:nil];
    [alert show];
    
    return NO;
}

#pragma mark - Animation method

- (void)dismissKeyboard
{
    [_textField resignFirstResponder];
}

- (void)animationMove:(UIView *)view
{
    UIView *selectedView = view;
    UIView *superviewSelectedView = [view superview];
    UIView *underSelectedView = [[view subviews] lastObject];
    
    if (![[selectedView subviews] count])
    {
        NSLog(@"This view without subview.");
    }
    else
    {
        //selectedView add on vc
        [[self view] addSubview:selectedView];
        [selectedView setCenter:[[self view] center]];
        //underSelectedView add on vc
        [[self view] addSubview:underSelectedView];
        [underSelectedView setCenter:[[self view]center]];

        //animating selected view and under selected view
        [self moveSelectedView:selectedView and:underSelectedView];

        //Looking top view
        UIView *someElseView = underSelectedView;
        
        while ([[someElseView subviews] count] > 0)
        {
            UIView *lastView = [[someElseView subviews] lastObject];
            someElseView = lastView;
        }
        //Move selected view and her subview on the center of the vc
        [UIView animateKeyframesWithDuration:2
                                       delay:2
                                     options:UIViewKeyframeAnimationOptionCalculationModeLinear
                                  animations:^{
                                      
                                      selectedView.center = CGPointMake(self.view.center.x, self.view.center.y);
                                      underSelectedView.center = CGPointMake(self.view.center.x, self.view.center.y);
                                      
                                  } completion:^(BOOL finished) {
                                      //Change hierarchy
                                      [underSelectedView removeFromSuperview];
                                      [selectedView removeFromSuperview];
                                      
                                      [superviewSelectedView addSubview:underSelectedView];
                                      [underSelectedView setCenter:CGPointMake(CGRectGetMidX(superviewSelectedView.bounds),
                                                                               CGRectGetMidY(superviewSelectedView.bounds))];
                                      
                                      
                                      [someElseView addSubview:selectedView];
                                      [selectedView setCenter:CGPointMake(CGRectGetMidX(someElseView.bounds),
                                                                          CGRectGetMidY(someElseView.bounds))];
                                  }];
        // Change view bounds
        [UIView animateKeyframesWithDuration:2
                                       delay:0
                                     options:UIViewKeyframeAnimationOptionCalculationModeLinear
                                  animations:^{
                                      
                                      [underSelectedView setBounds:selectedView.bounds];
                                      [selectedView setBounds:someElseView.bounds];
                                      [selectedView setBounds:CGRectMake(someElseView.bounds.origin.x,
                                                                         someElseView.bounds.origin.y,
                                                                         someElseView.bounds.size.width - 50,
                                                                         someElseView.bounds.size.height - 50)];
                                  } completion:^(BOOL finished) {
                                      //Completion Block
                                  }];
    }
}

- (void)moveSelectedView:(UIView *)firstView and:(UIView *)secondView
{
    //animating selected view and under selected view
    [UIView animateKeyframesWithDuration:2
                                   delay:0
                                 options:UIViewKeyframeAnimationOptionCalculationModeLinear
                              animations:^{
                                  //Move selected view and her subview in a different side
                                  firstView.center = CGPointMake(self.view.center.x + 130, self.view.center.y);
                                  secondView.center = CGPointMake(self.view.center.x - 130, self.view.center.y);
                                  
                              } completion:^(BOOL finished){
                                  //Change hierarchy
                                  [firstView removeFromSuperview];
                                  [secondView removeFromSuperview];
                                  
                                  [self.view addSubview:secondView];
                                  [self.view addSubview:firstView];
                              }];
}

#pragma mark - NIXViewDelegate method

- (void)didTapOnView:(NIXView *)view
{
    
}

@end
