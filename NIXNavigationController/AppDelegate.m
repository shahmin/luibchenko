//
//  AppDelegate.m
//  NIXNavigationController
//
//  Created by admin on 14.11.14.
//  Copyright (c) 2014 admin. All rights reserved.
//

#import "AppDelegate.h"
#import "NIXViewController.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    _window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [_window setBackgroundColor:[UIColor whiteColor]];
    [_window makeKeyAndVisible];
    
    _viewController = [[NIXViewController alloc] init];
    _navController = [[UINavigationController alloc] initWithRootViewController:_viewController];
    
    [_window setRootViewController:_navController];
    
    return YES;
}

@end
