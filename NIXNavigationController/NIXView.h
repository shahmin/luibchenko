//
//  NIXView.h
//  NIXNavigationController
//
//  Created by admin on 14.11.14.
//  Copyright (c) 2014 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NIXViewDelegate;

@interface NIXView : UIView

@property(nonatomic, assign) id<NIXViewDelegate> delegate;

@end

@protocol NIXViewDelegate <NSObject>

- (void)didTapOnView:(NIXView *)view;

@end